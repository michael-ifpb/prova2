#!/bin/bash

sair=fim 

menu ()
{

while true $x != "fim"
do

clear

echo "================================================"

echo "1)Informações sobre a memoria disponivel"
echo ""
echo "2)Informações sobre qual versão do sistema operacional"
echo ""
echo "3)Informações sobre o Ip da máquina"
echo ""
echo "4)Informações sobre a CPU"
echo ""
echo "5)Verificar se o arquivo existe"
echo ""
echo "6)Exibir diretorio atual"
echo ""
echo "7)Listagem dos arquivos do diretorio atual"
echo ""
echo "8)Mude o diretorio atual para o Home"
echo ""
echo "9)Sair do MENU" 
echo "================================================"


echo "Digite a opção desejada:"

read x

echo "Opção informada ($x)"

echo "================================================"


case "$x" in
	1)
	 echo "Segue informações sobre a mémoria da máquina"
	 free -h
	 sleep 5

	 echo "================================================" ;;

	 2)
       	 echo "A versão do seu sistema operacional é:"
	 lsb_release -a
	 sleep 5	 
	 
	 echo "================================================" ;;

	 3)
	 echo "Segue o Ip da sua máquina"
	 hostname -I
	 sleep 5

	 echo "================================================" ;;

	 4)
	 echo "Segue informações da sua CPU"
	 lscpu
	 sleep 5

	 echo "================================================" ;;

 	 5) 
	 echo "Verificando se o arquivo existe"
	 read arquivo
	 test -f $arquivo 
	 echo $?
	 sleep 5

	 echo "================================================" ;;

       	 6)
	 echo "Exibindo diretorio atual"
	 pwd
	 sleep 5
	 
	 echo "================================================" ;;

         7)
	 echo "Arquivos do diretorio atual"
	 ls 
	 sleep 5 

	 echo "================================================" ;;

	 8)
	 echo "Mudando para outro diretorio"
	 read diretorio
	 cd $diretorio
	 pwd
	 sleep 5

	 echo "================================================" ;;

         9)
	 echo "fechando o programa" 
	 sleep 5
	 clear;
	 exit;


	 echo "================================================";;

*)

	echo " Opção Inválida"





esac 
done 
}
menu 
	 
